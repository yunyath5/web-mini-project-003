import {
  CATEGORY_FETCH,
  CATEGORY_SUBMIT,
  CATEGORY_SUCCESS,
} from "../action/useCategoryAction";

const initState = {
  loading: false,
  categories: [],
  success: false,
  submitting: false,
};
export default function (state = initState, action = { payload: {} }) {
  switch (action.type) {
    case CATEGORY_FETCH:
      return {
        ...state,
        loading: true,
      };
    case CATEGORY_SUBMIT:
      return {
        ...state,
        submitting: true,
      };
    case CATEGORY_SUCCESS:
      return {
        ...state,
        ...action.payload,
        success: true,
        loading: false,
        submitting: false,
      };
    default:
      return state;
  }
}
