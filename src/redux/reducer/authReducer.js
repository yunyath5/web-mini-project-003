import {
  AUTH_FAIL,
  AUTH_LOGOUT,
  AUTH_SIGININ,
  AUTH_SUCCESS,
} from "../action/useAuthAction";

const initState = {
  isSignin: null,
  isSubmit: false,
  isSuccess: false,
  isFail: false,
  profile: {},
};

export default function (state = initState, action = { payload: {} }) {
  switch (action.type) {
    case AUTH_SIGININ:
      return {
        ...state,
        isSubmit: true,
      };
    case AUTH_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isSuccess: true,
        isSignin: true,
        isSubmit: false,
      };
    case AUTH_FAIL:
      return {
        ...state,
        isFail: true,
        isSubmit: false,
      };
    case AUTH_LOGOUT:
      return {
        ...initState,
      };
    default:
      return state;
  }
}
