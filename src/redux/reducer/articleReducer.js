import {
  ARTICLE_FETCH,
  ARTICLE_SUCCESS,
  ARTICLE_SUBMIT,
} from "../action/useArticleAction";

const initState = {
  loading: false,
  submitting: false,
  totalPage: 1,
  page: 1,
  articles: [],
};

export default function (state = initState, action = { payload: {} }) {
  switch (action.type) {
    case ARTICLE_FETCH:
      return {
        ...state,
        loading: true,
      };
    case ARTICLE_SUCCESS:
      return {
        ...state,
        ...action.payload,
        loading: false,
        submitting: false,
      };
    case ARTICLE_SUBMIT:
      return {
        ...state,
        submitting: true,
      };
    default:
      return state;
  }
}
