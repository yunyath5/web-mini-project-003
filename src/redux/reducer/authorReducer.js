import { AUTHOR_FETCH, AUTHOR_SUCCESS,AUTHOR_SUBMIT } from "../action/useAuthorAction";

const initState = {
  loading: false,
  submitting: false,
  authors: [],
  totalPage: 1,
  page: 1,
  success: false,
};
export default function (state = initState, action = { payload: {} }) {
  switch (action.type) {
    case AUTHOR_FETCH:
      return {
        ...state,
        loading: true,
      };
    case AUTHOR_SUBMIT:
      return {
        ...state,
        submitting:true,
      }
    case AUTHOR_SUCCESS:
      return {
        ...state,
        ...action.payload,
        success: true,
        loading: false,
        submitting:false,
      };
    default:
      return state;
  }
}
