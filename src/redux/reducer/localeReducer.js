import { LOCALE_CHANGE } from "../action/useLocaleAction";

const initState = {
  locale: localStorage.locale || "en",
};

export default function (state = initState, action = { payload: {} }) {
  switch (action.type) {
    case LOCALE_CHANGE:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}
