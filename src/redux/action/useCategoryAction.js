import { useDispatch, useSelector } from "react-redux";
import {
  create_category,
  delete_category,
  fetch_category,
  update_category,
} from "./../../service/category.service";
import ToastAlert from "../../component/ToastAlert";
import { bindActionCreators } from "redux";
import { useEffect } from "react";

export const CATEGORY_FETCH = "category-fetch";
export const CATEGORY_SUCCESS = "category-success";
export const CATEGORY_SUBMIT = "category-submit";

export default function () {
  const categoryState = useSelector((state) => state.category);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCategory());
  }, [categoryState.success]);

  function fetchCategory(params) {
    return async (dis) => {
      dis({
        type: CATEGORY_FETCH,
      });
      const { data } = await fetch_category(params);
      const payload = {
        categories: data.data,
      };
      dis({ type: CATEGORY_SUCCESS, payload });
    };
  }

  function saveCategory(category, isEdit, callback) {
    return async (dis) => {
      dis({ type: CATEGORY_SUBMIT });
      if (isEdit) {
        const { data } = await update_category(category);
        const payload = {
          categories: [
            ...categoryState.categories.map((item) =>
              item._id === category._id ? category : item
            ),
          ],
        };
        dis({ type: CATEGORY_SUCCESS, payload });
        ToastAlert.show(data.message);
      } else {
        const { data } = await create_category(category);
        const payload = {
          categories: [data.data, ...categoryState.categories],
        };
        dis({ type: CATEGORY_SUCCESS, payload });

        ToastAlert.show(data.message);
      }
      callback && callback();
    };
  }
  function deleteCategory(id) {
    return async (dis) => {
      const payload = {
        categories: [
          ...categoryState.categories.filter((item) => item._id !== id),
        ],
      };
      dis({ type: CATEGORY_SUCCESS, payload });
      delete_category(id);

      ToastAlert.show("Category delete successfully");
    };
  }

  const actions = {
    fetchCategory,
    saveCategory,
    deleteCategory,
  };
  return {
    ...categoryState,
    ...bindActionCreators(actions, dispatch),
  };
}
