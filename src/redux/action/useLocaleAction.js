import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import translate from "../../translate";

export const LOCALE_CHANGE = "lang-change";

export default function useLocaleAction() {
  const { locale, ...authState } = useSelector((state) => state.locale);
  const dispatch = useDispatch();

  function changeLocale(locale) {
    localStorage.setItem("locale", locale);
    return { type: LOCALE_CHANGE, payload: { locale } };
  }

  function tr(key, args) {
    const value = translate[key];
    if (value && value[locale]) {
      const name = value[locale];
      return name.replace(/{(\d+)}/g, (match, index) =>
        args ? args[index] || "" : ""
      );
    }
    return key;
  }

  const actions = {
    changeLocale,
  };

  return {
    tr,
    locale,
    ...authState,
    ...bindActionCreators(actions, dispatch),
  };
}
