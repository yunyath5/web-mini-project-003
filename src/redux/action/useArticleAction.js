import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetch_article,
  create_article,
  update_article,
  delete_article,
} from "../../service/article.service";
import { upload_image } from "../../service/upload.service";
import ToastAlert from "./../../component/ToastAlert";
import { useEffect } from "react";

export const ARTICLE_FETCH = "article-fetch";
export const ARTICLE_SUCCESS = "article-success";
export const ARTICLE_SUBMIT = "article-submit";

export default function () {
  const { page, totalPage, ...articleState } = useSelector(
    (state) => state.article
  );
  const dispatch = useDispatch();

  function fetchArticle(params = {}) {
    const search = params.search
      ? {
          title: params.search,
        }
      : {};
    delete params.search;

    return async (dis) => {
      dis({
        type: ARTICLE_FETCH,
      });
      const { data } = await fetch_article({ ...params, ...search });
      const payload = {
        articles: data.data,
        totalPage: data.total_page,
        page: data.page,
      };
      dis({ type: ARTICLE_SUCCESS, payload });
    };
  }

  function saveArticle(article, isEdit) {
    return async (dis) => {
      dis({ type: ARTICLE_SUBMIT });
      if (article.file) {
        const { data } = await upload_image(article.file);
        article.image = data.url;
      }
      const { data } = isEdit
        ? await update_article(article)
        : await create_article(article);
      ToastAlert.show(data.message);
      dis({ type: ARTICLE_SUCCESS });
    };
  }
  function deleteArticle(id) {
    return async (dis) => {
      const payload = {
        articles: [...articleState.articles.filter((item) => item._id !== id)],
      };
      dis({ type: ARTICLE_SUCCESS, payload });
      ToastAlert.show("Article delete successfully");
      delete_article(id);
    };
  }
  function hasNext() {
    return page < totalPage;
  }
  function hasPrev() {
    return page > 1;
  }

  const actions = {
    fetchArticle,
    saveArticle,
    deleteArticle,
  };
  return {
    hasNext,
    hasPrev,
    page,
    totalPage,
    ...articleState,
    ...bindActionCreators(actions, dispatch),
  };
}
