import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";

export const AUTH_SIGININ = "auth-signin";
export const AUTH_SUCCESS = "auth-success";
export const AUTH_FAIL = "auth-fail";
export const AUTH_LOGOUT = "auth-logout";

export default function () {
  const authState = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  function signin() {
    return {
      type: AUTH_SIGININ,
    };
  }
  function success(payload) {
    return {
      type: AUTH_SUCCESS,
      payload,
    };
  }
  function fail() {
    return {
      type: AUTH_FAIL,
    };
  }
  function logout() {
    return {
      type: AUTH_LOGOUT,
    };
  }

  const actions = {
    signin,
    success,
    fail,
    logout,
  };

  return {
    ...authState,
    ...bindActionCreators(actions, dispatch),
  };
}
