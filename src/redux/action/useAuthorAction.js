import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import {
  create_author,
  fetch_author,
  update_author,
  delete_author,
} from "./../../service/author.service";
import ToastAlert from "./../../component/ToastAlert";
import { upload_image } from "./../../service/upload.service";

export const AUTHOR_FETCH = "author-fetch";
export const AUTHOR_SUCCESS = "author-success";
export const AUTHOR_SUBMIT = "author-submit";

export default function () {
  const { page, totalPage, ...authorState } = useSelector(
    (state) => state.author
  );
  const dispatch = useDispatch();

  function hasNext() {
    return page < totalPage;
  }
  function hasPrev() {
    return page > 1;
  }
  function fetchAuthor(params) {
    return async (dis) => {
      dis({ type: AUTHOR_FETCH });
      const { data } = await fetch_author(params);
      const payload = {
        authors: data.data,
        totalPage: data.total_page,
        page: data.page,
      };
      dis({ type: AUTHOR_SUCCESS, payload });
    };
  }
  function saveAuthor(author, isEdit, callback) {
    return async (dis) => {
      dis({ type: AUTHOR_SUBMIT });
      if (author.file) {
        const { data } = await upload_image(author.file);
        author.image = data.url;
      }
      if (isEdit) {
        const { data } = await update_author(author);
        const payload = {
          authors: [
            ...authorState.authors.map((item) =>
              item._id === author._id ? author : item
            ),
          ],
        };
        dis({ type: AUTHOR_SUCCESS, payload });
        ToastAlert.show(data.message);
      } else {
        const { data } = await create_author(author);
        const payload = {
          authors: [data.data, ...authorState.authors],
        };
        dis({ type: AUTHOR_SUCCESS, payload });
        ToastAlert.show(data.message);
      }

      callback && callback();
    };
  }
  function deleteAuthor(id) {
    return async (dis) => {
      const payload = {
        authors: [...authorState.authors.filter((item) => item._id !== id)],
      };
      delete_author(id);
      dis({ type: AUTHOR_SUCCESS, payload });
      ToastAlert.show("Author delete successfully");
    };
  }

  const actions = {
    fetchAuthor,
    saveAuthor,
    deleteAuthor,
  };
  return {
    hasNext,
    hasPrev,
    page,
    totalPage,
    ...authorState,
    ...bindActionCreators(actions, dispatch),
  };
}
