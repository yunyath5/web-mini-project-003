import { createStore, combineReducers, applyMiddleware } from "redux";
import authReducer from "../reducer/authReducer";
import thunk from "redux-thunk";
import localeReducer from "../reducer/localeReducer";
import articleReducer from "../reducer/articleReducer";
import authorReducer from "../reducer/authorReducer";
import categoryReducer from "../reducer/categoryReducer";

const auth = authReducer;
const locale = localeReducer;
const article = articleReducer;
const author = authorReducer;
const category = categoryReducer;

const reducers = combineReducers({
  locale,
  auth,
  article,
  author,
  category,
});
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
export default createStoreWithMiddleware(reducers);
