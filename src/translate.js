export default {
  search: {
    en: "Search",
    km: "ស្វែងរក",
  },
  lang: {
    en: "Language",
    km: "ភាសា",
  },
  khmer: {
    en: "Khmer",
    km: "ខ្មែរ",
  },
  english: {
    en: "English",
    km: "អង់គ្លេស",
  },
  home: {
    en: "Home",
    km: "ទំព័រដើម",
  },
  article: {
    en: "Article",
    km: "អត្ថបទ",
  },
  category: {
    en: "Category",
    km: "ប្រភេទអត្ថបទ",
  },
  author: {
    en: "Author",
    km: "អ្នកនិពន្ធ",
  },
  delete: {
    en: "Delete",
    km: "លុប",
  },
  edit: {
    en: "Edit",
    km: "កែសម្រួល",
  },
  read: {
    en: "Read",
    km: "អាន",
  },
  pleaseLogin: {
    en: "Please Login",
    km: "សូមចូលប្រើប្រាស់",
  },
  signinWithGoogle: {
    en: "Signin with Google",
    km: "ចូលប្រើប្រាស់ជាមួយ Google",
  },
  logout: {
    en: "Logout",
    km: "ចាក់ចេញ",
  },
  required: {
    en: "Required",
    km: "ត្រូវការបំពេញ",
  },
  title: {
    en: "Title",
    km: "ចំណង់ជើង",
  },
  description: {
    en: "Description",
    km: "ការពិពណ៌នា",
  },
  save: {
    en: "Save",
    km: "រក្សាទុក",
  },
  create: {
    en: "Create",
    km: "បង្កើត",
  },
  image: {
    en: "Photo",
    km: "រូបភាព",
  },
  comment: {
    en: "Comments",
    km: "មតិ",
  },
  name: {
    en: "Name",
    km: "ឈ្មោះ",
  },
  email: {
    en: "Email",
    km: "អុីម៉ែល",
  },
  action: {
    en: "Action",
    km: "សកម្មភាព",
  },
  invalid_email: {
    en: "Invalid Email",
    km: "អុីម៉ែលមិនត្រឺមត្រូវ"
  }
};
