import api from "./api";

const url = '/category';

export function fetch_category(){
    return api.get(url);
}
export function create_category(category){
    return api.post(url,category);
}
export function update_category(category){
    return api.put(`${url}/${category._id}`,category);
}
export function delete_category(id){
    return api.delete(`${url}/${id}`);
}
