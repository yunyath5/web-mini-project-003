import queryString from "query-string";
import api from "./api";

const url = "/articles";

export function fetch_article(params = {}) {
  const query = queryString.stringify({
    page: 1,
    size: 6,
    ...params,
  });
  return api.get(`${url}?${query}`);
}

export function fetch_article_by_id(id) {
  return api.get(`${url}/${id}`);
}

export function create_article(article) {
  return api.post(url, article);
}

export function delete_article(id) {
  return api.delete(`${url}/${id}`);
}
export function update_article(article) {
  return api.patch(`${url}/${article._id}`, article);
}
