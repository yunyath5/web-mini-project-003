import api from "./api";

export function upload_image(file) {
  const form = new FormData();
  form.append("image", file);
  return api.post("/images", form);
}
