import queryString from 'query-string';
import api from './api';

const url = '/author';

export function fetch_author(param = { }){
    const query = queryString.stringify({
        page: 1,
        size: 6,
        ...param,
    });
    return api.get(`${url}?${query}`);
}

export function fetch_author_by_id(id){
    return api.get(`${url}/${id}`);
}

export function create_author(author){
    return api.post(url, author);

}
export function update_author(author){
    return api.put(`${url}/${author._id}`, author);

}
export function delete_author(id){
    return api.delete(`${url}/${id}`);
}