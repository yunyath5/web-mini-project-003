import Header from "../component/Header";
import { Container, Table, Button } from "react-bootstrap";
import useAuthorAction from "../redux/action/useAuthorAction";
import {
  Switch,
  useLocation,
  Route,
  useRouteMatch,
  Link,
} from "react-router-dom";
import { useEffect } from "react";
import useLocaleAction from "../redux/action/useLocaleAction";
import AuthorPagination from "../component/AuthorPagination";
import Loading from "../component/Loading";
import AuthorFormDialog from "../component/AuthorFormDialog";

export default function (props) {
  const { authors, fetchAuthor, loading, deleteAuthor } = useAuthorAction();
  const { tr } = useLocaleAction();
  const { search } = useLocation();
  const { url, path } = useRouteMatch();
  const query = new URLSearchParams(search);

  useEffect(() => {
    load();
  }, [search]);

  function load() {
    const params = {};
    if (query.get("page")) {
      params.page = query.get("page");
    }
    if (query.get("search")) {
      params.search = query.get("search");
    }
    fetchAuthor(params);
  }
  function renderRow(item, index) {
    return (
      <tr key={index}>
        <td>{item._id}</td>
        <td>{item.name}</td>
        <td>{item.email}</td>
        <td>
          <img style={{ height: 100 }} src={item.image} />
        </td>
        <td>
          <Link to={`${url}/post/${item._id}`}>
            <Button variant="warning" className="mr-2">
              {tr("edit")}
            </Button>
          </Link>

          <Button onClick={() => deleteAuthor(item._id)} variant="danger">
            {tr("delete")}
          </Button>
        </td>
      </tr>
    );
  }
  return (
    <div>
      <Header />
      <Container>
        {loading ? (
          <Loading />
        ) : (
          <>
            <h2 className="mb-4">{tr("author")}</h2>
            <Link to={`${url}/post`}>
              <Button className="mb-3">{tr("create")}</Button>
            </Link>
            <Switch>
              <Route path={`${path}/post/:id?`}>
                <AuthorFormDialog />
              </Route>
            </Switch>
            <Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>{tr("name")}</th>
                  <th>{tr("email")}</th>
                  <th>{tr("image")}</th>
                  <th>{tr("action")}</th>
                </tr>
              </thead>
              <tbody>{authors.map(renderRow)}</tbody>
            </Table>
            <AuthorPagination />
          </>
        )}
      </Container>
    </div>
  );
}
