import Header from "../component/Header";
import { Container, Form, Row, Col, Button } from "react-bootstrap";
import { useParams } from "react-router-dom";
import useLocaleAction from "../redux/action/useLocaleAction";
import { useEffect, useState } from "react";
import { fetch_author } from "./../service/author.service";
import { useFormik } from "formik";
import useArticleAction from "../redux/action/useArticleAction";
import * as yup from "yup";

export default function (props) {
  const [authors, setAuthors] = useState([]);
  const { articles, saveArticle, submitting } = useArticleAction();
  const { id } = useParams();
  const { tr } = useLocaleAction();
  const isEdit = !!id;
  const validationSchema = yup.object().shape({
    title: yup.string().required(tr("required")),
    description: yup.string().required(tr("required")),
    author_id: yup.string().required(tr("required")),
  });

  const { handleChange, setFieldValue, values, handleSubmit, errors } =
    useFormik({
      validateOnChange: false,
      validationSchema,
      onSubmit,
      initialValues: articles.find((item) => item._id === id) || {},
    });

  useEffect(() => {
    initial();
  }, []);

  function onSubmit(article) {
    saveArticle({ _id: id, ...article }, isEdit);
  }
  async function initial() {
    const { data } = await fetch_author({ page: null, size: null });
    setAuthors(data.data);
  }
  function renderAuthor() {
    return authors.map((item, index) => {
      return (
        <option key={index} value={item._id}>
          {item.name}
        </option>
      );
    });
  }
  return (
    <div>
      <Header />
      <Container>
        <h2 className="mb-4">
          {isEdit
            ? `${tr("edit")} ${tr("article")}`
            : `${tr("create")} ${tr("article")}`}
        </h2>
        <Form onSubmit={handleSubmit}>
          <Row>
            <Col className="col-8">
              <Form.Group controlId="title">
                <Form.Label>{tr("title")}</Form.Label>
                <Form.Control
                  onChange={handleChange}
                  value={values.title}
                  isInvalid={!!errors.title}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.title}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="author_id">
                <Form.Label>{tr("author")}</Form.Label>
                <Form.Control
                  onChange={handleChange}
                  value={values.author_id}
                  as="select"
                  isInvalid={!!errors.author_id}
                >
                  <option disabled selected>
                    {tr("author")}
                  </option>
                  {renderAuthor()}
                </Form.Control>
                <Form.Control.Feedback type="invalid">
                  {errors.author_id}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="description">
                <Form.Label>{tr("description")}</Form.Label>
                <Form.Control
                  as="textarea"
                  row={3}
                  isInvalid={!!errors.description}
                  onChange={handleChange}
                  value={values.description}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.description}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
            <Col>
              <div
                style={{
                  height: 200,
                  backgroundColor: "lightgray",
                  marginBottom: 10,
                  position: "relative",
                }}
              >
                <img
                  style={{ objectFit: "cover", height: "100%", width: "100%" }}
                  src={
                    values.file
                      ? URL.createObjectURL(values.file)
                      : values.image
                  }
                />
              </div>
              <Form.Group>
                <Form.Label>{tr("image")}</Form.Label>
                <Form.Control
                  type="file"
                  onChange={(e) => setFieldValue("file", e.target.files[0])}
                />
              </Form.Group>
            </Col>
          </Row>
          <Button disabled={submitting} type="submit" variant="primary">
            {isEdit ? tr("save") : tr("create")}
          </Button>
        </Form>
      </Container>
    </div>
  );
}
