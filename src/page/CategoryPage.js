import Header from "../component/Header";
import { Container, Table, Button } from "react-bootstrap";
import useCategoryAction from "../redux/action/useCategoryAction";
import { useRouteMatch, Link, Switch, Route } from "react-router-dom";
import useLocaleAction from "../redux/action/useLocaleAction";
import CategoryFormDialog from "../component/CategoryFormDialog";
import Loading from "../component/Loading";
export default function (props) {
  const { categories, deleteCategory,loading } = useCategoryAction();
  const { tr } = useLocaleAction();
  const { url, path } = useRouteMatch();

  function renderRow(item, index) {
    return (
      <tr key={index}>
        <td>{item._id}</td>
        <td>{item.name}</td>
        <td>
          <Link to={`${url}/post/${item._id}`}>
            <Button variant="warning" className="mr-2">
              {tr("edit")}
            </Button>
          </Link>

          <Button onClick={() => deleteCategory(item._id)} variant="danger">
            {tr("delete")}
          </Button>
        </td>
      </tr>
    );
  }

  return (
    <div>
      <Header />
      <Container>
        {loading ? (
          <Loading />
        ) : (
          <>
            <h2 className="mb-4">{tr("category")}</h2>
            <Link to={`${url}/post`}>
              <Button className="mb-3">{tr("create")}</Button>
            </Link>
            <Switch>
              <Route path={`${path}/post/:id?`}>
                <CategoryFormDialog />
              </Route>
            </Switch>
            <Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>{tr("name")}</th>
                  <th>{tr("action")}</th>
                </tr>
              </thead>
              <tbody>{categories.map(renderRow)}</tbody>
            </Table>
          </>
        )}
      </Container>
    </div>
  );
}
