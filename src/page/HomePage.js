import Header from "../component/Header";
import { Container, Row, Col, ButtonGroup, Button } from "react-bootstrap";
import ProfileCard from "../component/ProfileCard";
import useArticleAction from "../redux/action/useArticleAction";
import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import ArticleCard from "../component/ArticleCard";
import ArticlePagination from "../component/ArticlePagination";
import Loading from "../component/Loading";
import useCategoryAction from "../redux/action/useCategoryAction";
import useLocaleAction from "../redux/action/useLocaleAction";

export default function (props) {
  const { articles, fetchArticle, loading } = useArticleAction();
  const { categories } = useCategoryAction();
  const { tr } = useLocaleAction();
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  useEffect(() => {
    load();
  }, [search]);

  function load() {
    const params = {};
    if (query.get("page")) {
      params.page = query.get("page");
    }
    if (query.get("search")) {
      params.search = query.get("search");
    }
    fetchArticle(params);
  }
  function renderCategory() {
    return (
      <div>
        <h2>{tr("category")}</h2>
        <ButtonGroup className="my-3">
          {categories.map((item, index) => (
            <Button variant="secondary" key={index}>
              {item.name}
            </Button>
          ))}
        </ButtonGroup>
      </div>
    );
  }
  function renderArticleList() {
    return (
      <div>
        <Row>
          <Col className="col-12">{renderCategory()}</Col>
          {articles.map((item, index) => (
            <Col className="col-12 col-lg-4 col-md-6 mb-3" key={index}>
              <ArticleCard article={item} />
            </Col>
          ))}
        </Row>
        <ArticlePagination />
      </div>
    );
  }
  return (
    <div>
      <Header />
      <Container>
        <Row className="flex-wrap">
          <Col className="col-lg-3 col-md-4 col-sm-6">
            <ProfileCard />
          </Col>
          <Col>{loading ? <Loading /> : renderArticleList()}</Col>
        </Row>
      </Container>
    </div>
  );
}
