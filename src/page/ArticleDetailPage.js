import Header from "../component/Header";
import { Container, Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import Comments from "./../component/Comments";
import { fetch_article_by_id } from "../service/article.service";
import { useParams } from "react-router-dom";
import Loading from "../component/Loading";
import useLocaleAction from "../redux/action/useLocaleAction";
export default function (props) {
  const [article, setArticle] = useState({});
  const [loading, setLoading] = useState(false);
  const { id } = useParams();
  const { tr } = useLocaleAction();
  useEffect(() => {
    fetchData();
  }, [id]);
  async function fetchData() {
    setLoading(true);
    const { data } = await fetch_article_by_id(id);
    setArticle(data.data);
    setLoading(false);
  }
  return (
    <div>
      <Loading show={loading} />
      <Header />
      <Container>
        <Row>
          <Col className="col-7">
            <h3>{article.title}</h3>
            <div className="my-3">
              <img
                style={{ width: "100%", maxHeight: 400 }}
                src={article.image}
              />
            </div>
            <h6>{article.description}</h6>
          </Col>
          <Col>
            <h3>{tr("comment")}</h3>

            <Comments />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
