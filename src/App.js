import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "./page/HomePage";
import ArticlePage from "./page/PostArticlePage";
import AuthorPage from "./page/AuthorPage";
import CategoryPage from "./page/CategoryPage";
import ArticleDetailPage from "./page/ArticleDetailPage";
import ToastAlert from "./component/ToastAlert";

function App() {
  return (
    <Router>
      <ToastAlert />
      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>

        <Route path="/author">
          <AuthorPage />
        </Route>
        <Route path="/category">
          <CategoryPage />
        </Route>
        <Route path="/article/post/:id?">
          <ArticlePage />
        </Route>
        <Route path="/article/detail/:id">
          <ArticleDetailPage />
        </Route>
        <Route path="*">
          <div>404 Page not found.</div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
