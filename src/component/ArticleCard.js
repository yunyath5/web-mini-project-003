import { Card, Button } from "react-bootstrap";
import useLocaleAction from "../redux/action/useLocaleAction";
import { Link } from "react-router-dom";
import useArticleAction from "../redux/action/useArticleAction";
export default function (props) {
  const { article } = props;
  const { tr } = useLocaleAction();
  const { deleteArticle } = useArticleAction();
  return (
    <Card>
      <Card.Img style={{ height: 200 }} variant="top" src={article.image} />
      <Card.Body>
        <Card.Title>{article.title}</Card.Title>
        <Card.Text
          style={{
            maxHeight: 70,
            overflow: "hidden",
            textOverflow: "ellipsis",
          }}
        >
          {article.description}
        </Card.Text>
        <div className="d-flex">
          <Link to={`/article/detail/${article._id}`}>
            <Button className="mr-2" size="sm" variant="primary">
              {tr("read")}
            </Button>
          </Link>
          <Link to={`/article/post/${article._id}`}>
            <Button className="mr-2" size="sm" variant="warning">
              {tr("edit")}
            </Button>
          </Link>
          <Button
            onClick={() => deleteArticle(article._id)}
            size="sm"
            variant="danger"
          >
            {tr("delete")}
          </Button>
        </div>
      </Card.Body>
    </Card>
  );
}
