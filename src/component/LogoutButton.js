import { Button } from "react-bootstrap";
import { GoogleLogout } from "react-google-login";
import useAuthAction from "../redux/action/useAuthAction";
import useLocaleAction from "../redux/action/useLocaleAction";

export default function (props) {
  const { logout, fail, isSubmit } = useAuthAction();
  const { tr } = useLocaleAction();

  function onSuccess() {
    logout();
  }
  function onFailure(er) {
    fail();
  }
  return (
    <GoogleLogout
      clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
      render={(p) => (
        <Button
          variant="danger"
          disabled={p.disabled || isSubmit}
          onClick={p.onClick}
        >
          {tr("logout")}
        </Button>
      )}
      onLogoutSuccess={onSuccess}
      onFailure={onFailure}
    />
  );
}
