import { Modal, Form, Row, Col, Button } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import useLocaleAction from "../redux/action/useLocaleAction";
import { useFormik } from "formik";
import * as yup from "yup";
import useCategoryAction from "../redux/action/useCategoryAction";
export default function (props) {
  const { tr } = useLocaleAction();
  const { categories, submitting, saveCategory } = useCategoryAction();
  const { id } = useParams();
  const isEdit = !!id;
  const history = useHistory();
  const validationSchema = yup.object().shape({
    name: yup.string().required(tr("required")),
  });
  const { handleSubmit, handleChange, values, errors } = useFormik({
    validateOnChange: false,
    onSubmit,
    initialValues: categories.find((item) => item._id === id) || {},
    validationSchema,
  });
  function onSubmit(category) {
    saveCategory({ _id: id, ...category }, isEdit, () => history.goBack());
  }
  return (
    <Modal
      size="lg"
      backdrop="static"
      keyboard={false}
      show={true}
      onHide={() => history.goBack()}
    >
      <Form onSubmit={handleSubmit}>
        <Modal.Header closeButton={!submitting}>
          <Modal.Title>{`${tr(isEdit ? "edit" : "create")} ${tr(
            "category"
          )}`}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group controlId="name">
            <Form.Label>{tr("name")}</Form.Label>
            <Form.Control
              isInvalid={!!errors.name}
              value={values.name}
              onChange={handleChange}
            />
            <Form.Control.Feedback type="invalid">
              {errors.name}
            </Form.Control.Feedback>
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button disabled={submitting} type="submit">
            {tr(isEdit ? "save" : "create")}
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}
