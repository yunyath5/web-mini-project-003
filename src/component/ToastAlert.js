import React, { createRef, useState } from "react";
import { Toast } from "react-bootstrap";

export default class ToastAlert extends React.Component {
  static alertRef = createRef();
  static show(message, variant) {
    ToastAlert.alertRef.current?.show(message, variant);
  }
  static hide() {
    ToastAlert.alertRef.current?.hide();
  }
  render() {
    return (
      <div
        style={{
          position: "absolute",
          top: 30,
          right: 30,
          zIndex: 999,
        }}
      >
        <Alert ref={ToastAlert.alertRef} />
      </div>
    );
  }
}

class Alert extends React.Component {
  state = {
    open: false,
    message: undefined,
    variant: undefined,
  };
  show = (message, variant) => {
    this.setState({
      open: true,
      message,
      variant,
    });
  };
  hide = () => {
    this.setState({
      open: false,
    });
  };
  render() {
    const { open, message } = this.state;
    return (
      <Toast style={{ minWidth: 300 }} show={open} autohide onClose={this.hide}>
        <Toast.Header>
          <strong className="mr-auto">AWS</strong>
          <small>just now</small>
        </Toast.Header>
        <Toast.Body>{message}</Toast.Body>
      </Toast>
    );
  }
}
