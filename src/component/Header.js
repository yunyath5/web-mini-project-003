import {
  Button,
  Container,
  Form,
  FormControl,
  Nav,
  Navbar,
  NavDropdown,
} from "react-bootstrap";
import { Link, NavLink, useLocation } from "react-router-dom";
import useLocaleAction from "../redux/action/useLocaleAction";

export default function (props) {
  const { changeLocale, locale, tr } = useLocaleAction();
  const { search } = useLocation();
  const query = new URLSearchParams(search);

  return (
    <Navbar bg="primary" variant="dark" expand="lg" className="mb-4">
      <Container>
        <Navbar.Brand as={Link} to="/">
          AWS Redux
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link exact as={NavLink} to="/">
              {tr("home")}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/article/post">
              {tr("article")}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/author">
              {tr("author")}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/category">
              {tr("category")}
            </Nav.Link>
            <NavDropdown title={tr("lang")} onSelect={changeLocale}>
              <NavDropdown.Item active={locale === "en"} eventKey="en">
                {tr("english")}
              </NavDropdown.Item>
              <NavDropdown.Item active={locale === "km"} eventKey="km">
                {tr("khmer")}
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Form inline>
            <FormControl
              defaultValue={query.get("search")}
              type="text"
              name="search"
              placeholder={tr("search")}
              className="mr-sm-2"
            />
            <Button variant="light">{tr("search")}</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
