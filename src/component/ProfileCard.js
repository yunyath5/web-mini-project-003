import { Card } from "react-bootstrap";
import useAuthAction from "../redux/action/useAuthAction";
import useLocaleAction from "../redux/action/useLocaleAction";
import LoginButton from "./LoginButton";
import LogoutButton from "./LogoutButton";
export default function (props) {
  const { isSignin, profile } = useAuthAction();
  const { tr } = useLocaleAction();

  return (
    <Card>
      <Card.Img
        variant="top"
        src={
          profile.imageUrl ||
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPyGNr2qL63Sfugk2Z1-KBEwMGOfycBribew&usqp=CAU"
        }
      />
      <Card.Body>
        <Card.Title>{profile.name || tr("pleaseLogin")}</Card.Title>
        {profile.email && <Card.Text>{profile.email}</Card.Text>}
        {isSignin ? <LogoutButton /> : <LoginButton />}
      </Card.Body>
    </Card>
  );
}
