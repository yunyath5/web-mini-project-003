import GoogleLogin from "react-google-login";
import { Button } from "react-bootstrap";
import useAuthAction from "../redux/action/useAuthAction";
import useLocaleAction from "../redux/action/useLocaleAction";
export default function (props) {
  const { success, fail, signin, isSubmit } = useAuthAction();
  const { tr } = useLocaleAction();

  function onSuccess(res) {
    const { profileObj } = res;
    success({ profile: profileObj });
  }
  function onFailure(err) {
    fail();
  }
  return (
    <GoogleLogin
      clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
      render={(p) => (
        <Button
          variant="primary"
          disabled={p.disabled || isSubmit}
          onClick={p.onClick}
        >
          {tr("signinWithGoogle")}
        </Button>
      )}
      onRequest={signin}
      onSuccess={onSuccess}
      onFailure={onFailure}
      cookiePolicy="single_host_origin"
    />
  );
}
