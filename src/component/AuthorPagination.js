import { Pagination } from "react-bootstrap";
import { useHistory, useLocation } from "react-router-dom";
import useAuthorAction from "../redux/action/useAuthorAction";
export default function (props) {
  const { page, totalPage, hasNext, hasPrev } = useAuthorAction();
  const { search, pathname } = useLocation();
  const query = new URLSearchParams(search);
  const history = useHistory();
  function goto(page) {
    query.set("page", page);
    const path = `${pathname}?${query.toString()}`;
    history.push(path);
  }
  function renderItem() {
    const items = [];
    for (let i = 1; i <= totalPage; i++) {
      items.push(
        <Pagination.Item
          onClick={() => (i === page ? null : goto(i))}
          key={i}
          active={i === page}
        >
          {i}
        </Pagination.Item>
      );
    }
    return items;
  }
  return (
    <div className="d-flex justify-content-center mt-4">
      <Pagination>
        <Pagination.Prev
          title="Previous"
          disabled={!hasPrev()}
          onClick={() => goto(page - 1)}
        />

        {renderItem()}
        <Pagination.Next
          title="Next"
          disabled={!hasNext()}
          onClick={() => goto(page + 1)}
        />
      </Pagination>
    </div>
  );
}
