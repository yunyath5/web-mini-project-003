import  {Modal} from 'react-bootstrap';
export default function (props) {
  return (
    <Modal
      contentClassName="tran-content"
      animation={false}
      centered
      show={true}
    >
      <div className="sk-folding-cube">
        <div className="sk-cube1 sk-cube"></div>
        <div className="sk-cube2 sk-cube"></div>
        <div className="sk-cube4 sk-cube"></div>
        <div className="sk-cube3 sk-cube"></div>
      </div>
    </Modal>
  );
}
