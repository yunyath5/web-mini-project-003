import { Modal, Form, Row, Col, Button } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import useLocaleAction from "../redux/action/useLocaleAction";
import { useFormik } from "formik";
import * as yup from "yup";
import useAuthorAction from "../redux/action/useAuthorAction";
export default function (props) {
  const { tr } = useLocaleAction();
  const { authors, submitting, saveAuthor } = useAuthorAction();
  const { id } = useParams();
  const isEdit = !!id;
  const history = useHistory();
  const validationSchema = yup.object().shape({
    name: yup.string().required(tr("required")),
    email: yup.string().email(tr("invalid_email")).required(tr("required")),
  });
  const { handleSubmit, handleChange, setFieldValue, values, errors } =
    useFormik({
      validateOnChange: false,
      onSubmit,
      initialValues: authors.find((item) => item._id === id) || {},
      validationSchema,
    });
  function onSubmit(author) {
    saveAuthor({ _id: id, ...author }, isEdit, () => history.goBack());
  }
  return (
    <Modal
      size="lg"
      backdrop="static"
      keyboard={false}
      show={true}
      onHide={() => history.goBack()}
    >
      <Form onSubmit={handleSubmit}>
        <Modal.Header closeButton={!submitting}>
          <Modal.Title>{`${tr(isEdit ? "edit" : "create")} ${tr(
            "author"
          )}`}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col className="col-8">
              <Form.Group controlId="name">
                <Form.Label>{tr("name")}</Form.Label>
                <Form.Control
                  isInvalid={!!errors.name}
                  value={values.name}
                  onChange={handleChange}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.name}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="email">
                <Form.Label>{tr("email")}</Form.Label>
                <Form.Control
                  value={values.email}
                  isInvalid={!!errors.email}
                  onChange={handleChange}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.email}
                </Form.Control.Feedback>
              </Form.Group>
            </Col>
            <Col>
              <div
                style={{
                  height: 200,
                  backgroundColor: "lightgray",
                  marginBottom: 10,
                  position: "relative",
                }}
              >
                <img
                  style={{ objectFit: "cover", height: "100%", width: "100%" }}
                  src={
                    values.file
                      ? URL.createObjectURL(values.file)
                      : values.image
                  }
                />
              </div>
              <Form.Group>
                <Form.Label>{tr("image")}</Form.Label>
                <Form.Control
                  type="file"
                  onChange={(e) => setFieldValue("file", e.target.files[0])}
                />
              </Form.Group>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button disabled={submitting} type="submit">
            {tr(isEdit ? "save" : "create")}
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
}
